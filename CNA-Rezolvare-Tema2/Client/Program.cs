﻿using Grpc.Core;
using System;
using System.Collections.Generic;

namespace Client
{
    class Program
    {
        static void Main(string[] args)
        {
            const string Host = "localhost";
            const int Port = 16842;

            var channel = new Channel($"{Host}:{Port}", ChannelCredentials.Insecure);
            string date;

            do
            {
                Console.WriteLine("Please enter a date (MM/DD/YYYY): ");
                date = Console.ReadLine();

            } while (!Validate(date));

            var client = new Generated.ZodiacOperationService.ZodiacOperationServiceClient(channel);

            var response = client.Zodiac(new Generated.ZodiacRequest
            {
                Message = date
            });

            Console.WriteLine("\nResponse: Zodiac Sign = {0}", response.Response);

            // Shutdown
            channel.ShutdownAsync().Wait();
            Console.WriteLine("Press any key to exit...");
            Console.ReadKey();
        }

        private static bool Validate(string date)
        {
            // Check for length
            if (date.Length != 10)
            {
                Console.WriteLine("Invalid date!");
                return false;
            }

            //Check if date contains another character
            List<char> validChars = new List<char>() { '/', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' };
            foreach (char c in date)
            {
                if (!validChars.Contains(c))
                {
                    Console.WriteLine("Invalid date!");
                    return false;
                }
            }

            //Verify the format "mm/dd/yyyy"
            if (date[2] != '/' || date[5] != '/')
            {
                Console.WriteLine("Invalid date!");
                return false;
            }


            //Verify if mm, dd, yyyy are numbers
            if (!(Char.IsDigit(date[0]) && Char.IsDigit(date[1])))
            {
                Console.WriteLine("Invalid date!");
                return false;
            }
            if (!(Char.IsDigit(date[3]) && Char.IsDigit(date[4])))
            {
                Console.WriteLine("Invalid date!");
                return false;
            }
            if (!(Char.IsDigit(date[6]) && Char.IsDigit(date[7])))
            {
                Console.WriteLine("Invalid date!");
                return false;
            }
            if (!(Char.IsDigit(date[8]) && Char.IsDigit(date[9])))
            {
                Console.WriteLine("Invalid date!");
                return false;
            }

            //Transform date string to DateTime
            try
            {
                DateTime dateTime = DateTime.Parse(date);
                //Console.WriteLine(dateTime);
            }
            catch (Exception)
            {
                Console.WriteLine("Invalid date!");
                return false;
            }

            return true;
        }
    }
}
