﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Server
{
    class Server : IDisposable
    {
        public static List<ZodiacSign> signs;

        public Grpc.Core.Server GrpcServer { get; private set; }

        public Action CloseServerAction { get; set; }

        public IEnumerable<Grpc.Core.ServerServiceDefinition> Services
        {
            get
            {
                yield return Generated.ZodiacOperationService.BindService(new ZodiacOperationService());
            }
        }

        public Server(string host, int port)
        {
            signs = new List<ZodiacSign>();

            ReadSignsFromFile("..\\..\\list.txt");

            GrpcServer = new Grpc.Core.Server()
            {
                Ports = { new Grpc.Core.ServerPort(host, port, Grpc.Core.ServerCredentials.Insecure) }
            };

            LoadServices();
        }

        public void Start()
        {
            GrpcServer.Start();

            Console.WriteLine(string.Format("Server started ({0}:{1}).", Configuration.HOST, Configuration.PORT));
        }

        private void LoadServices()
        {
            Services.ToList().ForEach(service => GrpcServer.Services.Add(service));
        }

        public void Dispose()
        {
            CloseServerAction.Invoke();
            GrpcServer.ShutdownAsync().Wait();
            var port = GrpcServer.Ports.FirstOrDefault();
            Console.WriteLine("Server closed ({0}:{1}).", Configuration.HOST, Configuration.PORT);
        }
        private void ReadSignsFromFile(string path)
        {
            string line;
            System.IO.StreamReader file = new System.IO.StreamReader(path);

            while ((line = file.ReadLine()) != null)
            {
                string[] sign = line.Split();

                string name = sign[0];

                int beginMonth = Int32.Parse(sign[1]);
                int beginDay = Int32.Parse(sign[2]);

                int endMonth = Int32.Parse(sign[4]);
                int endDay = Int32.Parse(sign[5]);

                signs.Add(new ZodiacSign(name, new ZodiacSign.Date(beginMonth, beginDay), new ZodiacSign.Date(endMonth, endDay)));
            }
        }
    }
}