﻿using Generated;
using Grpc.Core;
using System;
using System.Threading.Tasks;

namespace Server
{
    internal class ZodiacOperationService : Generated.ZodiacOperationService.ZodiacOperationServiceBase

    {
        public override Task<ZodiacResponse> Zodiac(ZodiacRequest request, ServerCallContext context)
        {
            System.Console.Write("Date: ");
            System.Console.WriteLine(request.Message);

            string response = "";

            DateTime dateTime = DateTime.Parse(request.Message);
            int month = dateTime.Month;
            int day = dateTime.Day;

            foreach (ZodiacSign sign in Server.signs)
            {
                if (sign.Begin.Month == month)
                {
                    if (sign.Begin.Day <= day)
                    {
                        response = sign.Name;
                        break;
                    }
                }
                else if (sign.End.Month == month)
                {
                    if (sign.End.Day >= day)
                    {
                        response = sign.Name;
                        break;
                    }
                }
            }

            //System.Console.Write("Zodiac Sign: ");
            //System.Console.WriteLine(response);

            return Task.FromResult(new ZodiacResponse() { Response = response });
        }
    }
}


