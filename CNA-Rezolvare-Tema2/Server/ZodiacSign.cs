﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Server
{
    class ZodiacSign
    {
        public string Name { get; private set; }
        public Date Begin { get; private set; }
        public Date End { get; private set; }

        public ZodiacSign(string name, Date begin, Date end)
        {
            Name = name;
            Begin = begin;
            End = end;
        }

        public class Date
        {
            public int Month { get; private set; }
            public int Day { get; private set; }

            public Date(int month, int day)
            {
                Month = month;
                Day = day;
            }
        }
    }
}
